  <?php 
error_reporting(0);
 require "db_connection.php"; 
?>
  <link rel="stylesheet" href="style/bootstrap.min.css">
          <link rel="stylesheet" href="style/todolist.css">           
        <script src="js/jquery-2.2.4.min.js" type="text/javascript"></script>
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
     	 <script src="js/list.js" type="text/javascript"></script>
        <meta charset="UTF-8">
    <title>Document</title>
     
             
<body>
<div class="container">
	<h1 class="headtext text-center">To Do List</h1>
</div>
<div class="container" style="margin-top: 10px">
<div class="divlogin"><h2 class="userlogin"><?= $_SESSION['logged_user'];?></h2></div>
<div class="div_for_table">
<table class="table table-bordered">
	<thead>
		<tr class="danger">
			<th>ID</th>
			<th>Login</th>
			<th>Task</th>
			<th>Date</th>
		</tr>
	</thead>
	<tbody id="tasklist">
				
	</tbody>
</table>
</div>
<button class="btn btn-info" data-toggle="modal" data-target="#addModal">Add</button>
<button id="editbutton" class="btn" data-toggle="modal" data-target="#editModal">Edit</button>
<button id="delete" class="btn btn-danger">Delete</button>
<button id="dele" class="btn btn-danger">Delete All</button>
</div>
<div id="addModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add New Task</h4>
      </div>
      <div class="modal-body">
       <div class="form-group">
  <label for="textarea">Task:</label>
  <textarea id="textarea" class="form-control" rows="5"></textarea>
</div>
      </div>
      <div class="modal-footer">
      	 <button id="save" type="button" class="btn btn-success" data-dismiss="modal">Save</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Edit Task</h4>
      </div>
      <div class="modal-body">
       <div class="form-group">
  <label for="editArea">Task:</label>
  <textarea id="editArea" class="form-control" rows="5"></textarea>
</div>
      </div>
      <div class="modal-footer">
      	 <button id="saveEdit" type="button" class="btn btn-success" data-dismiss="modal">Edit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
</body>
